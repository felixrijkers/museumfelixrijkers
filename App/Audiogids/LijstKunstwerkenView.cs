﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Audiogids
{
  public partial class LijstKunstwerkenView : UserControl, Observer
  {
    public LijstKunstwerkenView()
    {
      InitializeComponent();
    }

    public void ModelChanged()
    {
     this.Visible = Controller.Instance.KunstwerkLijstViewVisibility();
     lstKunstwerken.Items.Clear();

     if (this.Visible)
     {
       int aantalKunstwerken = Controller.Instance.getNummer();
       int aantalGevonden = 0;
       Controller.Instance.setNummer(0);

       for (int i = 0; i < aantalKunstwerken; i++)
       {
         
         string zoekterm = Controller.Instance.GetZoekterm();
         string kunstenaar = Controller.Instance.GetKunstenaarNaam();
         string kunstwerk = Controller.Instance.GetKunstwerkNaam();
 
         if(zoekterm == kunstenaar)
         {
           lstKunstwerken.Items.Add(kunstwerk);
           aantalGevonden = aantalGevonden + 1;
           lblArtiestNaam.Text = Controller.Instance.GetKunstenaarNaam();
           lblGeboortejaar.Text = Controller.Instance.GetKunstenaarGeboortejaar();
         }
         
         Controller.Instance.setNummer(i+1);

       }
       
       if(aantalGevonden == 0)
       {
         lstKunstwerken.Items.Add("Geen resultaten");
         lblArtiestNaam.Text = "Geen resultaat";
         lblGeboortejaar.Text = "";
         
       }
       
     }
    }

    private void LijstKunstwerkenView_Load(object sender, EventArgs e)
    {

    }

    private void btnLijstTerugIndex_Click(object sender, EventArgs e)
    {
      Controller.Instance.btnHomeClicked();
    }
  }
}
