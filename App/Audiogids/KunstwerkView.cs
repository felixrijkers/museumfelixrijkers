﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Audiogids
{
  public partial class KunstwerkView : UserControl, Observer
  {
    public KunstwerkView()
    {
      InitializeComponent();
    }

    public KunstwerkView(Kunstwerk kunstwerk)
    {
      InitializeComponent();
    }

    public void ModelChanged()
    {
      this.Visible = Controller.Instance.KunstwerkViewVisibility();

      if (this.Visible)
      {
        lblKunstwerkNr.Text = Controller.Instance.GetKunstwerkNr();
        txtNaamKunstwerk.Text = Controller.Instance.GetKunstwerkNaam();
        txtKunstenaar.Text = Controller.Instance.GetKunstenaarNaam();
        txtJaar.Text = Controller.Instance.GetKunstenaarGeboortejaar();
        rtbBeschrijving.Text = Controller.Instance.GetKunstwerkBeschrijving();
      }
    }

    private void btnVorige_Click(object sender, EventArgs e)
    {
      Controller.Instance.btnHomeClicked();
    }

    private void btnKunstwerkTerugIndex_Click(object sender, EventArgs e)
    {
      Controller.Instance.btnHomeClicked();
    }

    private void KunstwerkView_Load(object sender, EventArgs e)
    {

    }

    private void KunstwerkView_Load_1(object sender, EventArgs e)
    {

    }
  }
}
