﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Audiogids
{
  public partial class Form1 : Form, Observer
  {
    IndexView indexView;
    KunstwerkView kunstwerkView;
    LijstKunstwerkenView lijstView; 

    public Form1()
    {

      InitializeComponent();
      
      indexView = Controller.Instance.getIndexView();
      indexView.Location = new Point(0, 0);
      Controls.Add(indexView);
      indexView.Visible = true;

      kunstwerkView = Controller.Instance.getKunstwerkView();
      kunstwerkView.Location = new Point(0, 0);
      Controls.Add(kunstwerkView);
      kunstwerkView.Visible = false;

      lijstView = Controller.Instance.getLijstView();
      lijstView.Location = new Point(0, 0);
      Controls.Add(lijstView);
      lijstView.Visible = false ;
      
   
    }

    public void ModelChanged()
    {
    }

    private void Form1_Load(object sender, EventArgs e)
    {

    }
  }
}
