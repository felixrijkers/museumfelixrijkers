﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audiogids
{
  public class Archief : Observable, Observer
  {
    // Variabelen
    private Kunstwerk[] kunstwerken;
    private Kunstenaar[] kunstenaars;

    private bool indexViewIsVisible = true;
    private bool kunstwerkViewIsVisible = false;
    private bool lijstViewIsVisible = false;

    public int nummer;
    public string naam;
    public int aantalrecords;

    // Properties
    public bool IndexViewIsVisible
    {
      get { return indexViewIsVisible; }
      set 
      { 
        this.indexViewIsVisible = value;
        NotifyObservers();
      }
    }

    public bool KunstwerkViewIsVisible
    {
      get { return kunstwerkViewIsVisible; }
      set
      {
        this.kunstwerkViewIsVisible = value;
        NotifyObservers();
      }
    }

    public bool LijstViewIsVisible
    {
      get { return lijstViewIsVisible; }
      set
      {
        this.lijstViewIsVisible = value;
        NotifyObservers();
      }
    }

    // Constructor
    public Archief()
    {
      
      int aantalKunstwerken = 20;
      aantalrecords = aantalKunstwerken;
      
      Random rnd = new Random(); 

      kunstwerken = new Kunstwerk[aantalKunstwerken];
      kunstenaars = new Kunstenaar[aantalKunstwerken];

      for (int i = 0; i < aantalKunstwerken; i++)
      {
        kunstenaars[i] = new Kunstenaar("Kunstenaar " + i, i);
        kunstwerken[i] = new Kunstwerk(i, "Kunstwerk " + i, "Beschrijving " + i, kunstenaars[i]);
      }
    
    }

    // Methods
    public Kunstwerk GetKunstwerk()
    {
      return this.kunstwerken[this.nummer];
    }

    public Kunstenaar[] GetKunstenaars()
    {
      return this.kunstenaars;
    }

    public Kunstwerk[] GetKunstwerken(Kunstenaar artiest)
    {
      return this.kunstwerken;
    }

    public void ModelChanged()
    {
      NotifyObservers();
    }

  }
}
