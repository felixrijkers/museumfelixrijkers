﻿namespace Audiogids
{
  partial class IndexView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblIndexTitel = new System.Windows.Forms.Label();
      this.lblIndexNummerKunstwerk = new System.Windows.Forms.Label();
      this.txtKunstwerkNr = new System.Windows.Forms.TextBox();
      this.btnExtraInfoKunstwerk = new System.Windows.Forms.Button();
      this.lblIndexArtiest = new System.Windows.Forms.Label();
      this.txtArtiestNaam = new System.Windows.Forms.TextBox();
      this.btnLijstKunstwerken = new System.Windows.Forms.Button();
      this.grpNotifications = new System.Windows.Forms.GroupBox();
      this.lblNotificatie = new System.Windows.Forms.Label();
      this.grpNotifications.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblIndexTitel
      // 
      this.lblIndexTitel.Font = new System.Drawing.Font("Calibri", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblIndexTitel.Location = new System.Drawing.Point(23, 5);
      this.lblIndexTitel.Name = "lblIndexTitel";
      this.lblIndexTitel.Size = new System.Drawing.Size(297, 45);
      this.lblIndexTitel.TabIndex = 0;
      this.lblIndexTitel.Text = "MSK AUDIOGIDS";
      this.lblIndexTitel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lblIndexTitel.Click += new System.EventHandler(this.lblIndexTitel_Click);
      // 
      // lblIndexNummerKunstwerk
      // 
      this.lblIndexNummerKunstwerk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblIndexNummerKunstwerk.Location = new System.Drawing.Point(26, 66);
      this.lblIndexNummerKunstwerk.Name = "lblIndexNummerKunstwerk";
      this.lblIndexNummerKunstwerk.Size = new System.Drawing.Size(304, 85);
      this.lblIndexNummerKunstwerk.TabIndex = 1;
      this.lblIndexNummerKunstwerk.Text = "Vind kunstwerk:";
      this.lblIndexNummerKunstwerk.Click += new System.EventHandler(this.lblIndexTitle_Click);
      // 
      // txtKunstwerkNr
      // 
      this.txtKunstwerkNr.Font = new System.Drawing.Font("Consolas", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtKunstwerkNr.Location = new System.Drawing.Point(24, 98);
      this.txtKunstwerkNr.Name = "txtKunstwerkNr";
      this.txtKunstwerkNr.Size = new System.Drawing.Size(289, 54);
      this.txtKunstwerkNr.TabIndex = 2;
      this.txtKunstwerkNr.TextChanged += new System.EventHandler(this.txtKunstwerkNr_TextChanged);
      // 
      // btnExtraInfoKunstwerk
      // 
      this.btnExtraInfoKunstwerk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnExtraInfoKunstwerk.Location = new System.Drawing.Point(23, 157);
      this.btnExtraInfoKunstwerk.Name = "btnExtraInfoKunstwerk";
      this.btnExtraInfoKunstwerk.Size = new System.Drawing.Size(289, 32);
      this.btnExtraInfoKunstwerk.TabIndex = 3;
      this.btnExtraInfoKunstwerk.Text = "Zoek";
      this.btnExtraInfoKunstwerk.UseVisualStyleBackColor = true;
      this.btnExtraInfoKunstwerk.Click += new System.EventHandler(this.btnBekijkExtraInfo_Click);
      // 
      // lblIndexArtiest
      // 
      this.lblIndexArtiest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblIndexArtiest.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
      this.lblIndexArtiest.Location = new System.Drawing.Point(389, 56);
      this.lblIndexArtiest.Name = "lblIndexArtiest";
      this.lblIndexArtiest.Size = new System.Drawing.Size(304, 31);
      this.lblIndexArtiest.TabIndex = 7;
      this.lblIndexArtiest.Text = "Vind werken van artiest:";
      this.lblIndexArtiest.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // txtArtiestNaam
      // 
      this.txtArtiestNaam.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtArtiestNaam.Location = new System.Drawing.Point(387, 98);
      this.txtArtiestNaam.Name = "txtArtiestNaam";
      this.txtArtiestNaam.Size = new System.Drawing.Size(289, 53);
      this.txtArtiestNaam.TabIndex = 8;
      this.txtArtiestNaam.TextChanged += new System.EventHandler(this.txtArtiestNaam_TextChanged);
      // 
      // btnLijstKunstwerken
      // 
      this.btnLijstKunstwerken.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnLijstKunstwerken.Location = new System.Drawing.Point(387, 157);
      this.btnLijstKunstwerken.Name = "btnLijstKunstwerken";
      this.btnLijstKunstwerken.Size = new System.Drawing.Size(289, 32);
      this.btnLijstKunstwerken.TabIndex = 9;
      this.btnLijstKunstwerken.Text = "Zoek";
      this.btnLijstKunstwerken.UseVisualStyleBackColor = true;
      this.btnLijstKunstwerken.Click += new System.EventHandler(this.btnLijstKunstwerken_Click);
      // 
      // grpNotifications
      // 
      this.grpNotifications.Controls.Add(this.lblNotificatie);
      this.grpNotifications.Location = new System.Drawing.Point(24, 200);
      this.grpNotifications.Name = "grpNotifications";
      this.grpNotifications.Size = new System.Drawing.Size(652, 85);
      this.grpNotifications.TabIndex = 10;
      this.grpNotifications.TabStop = false;
      this.grpNotifications.Text = "Notificaties";
      // 
      // lblNotificatie
      // 
      this.lblNotificatie.AutoSize = true;
      this.lblNotificatie.Location = new System.Drawing.Point(11, 37);
      this.lblNotificatie.Name = "lblNotificatie";
      this.lblNotificatie.Size = new System.Drawing.Size(70, 13);
      this.lblNotificatie.TabIndex = 0;
      this.lblNotificatie.Text = "lblNotification";
      this.lblNotificatie.Visible = false;
      // 
      // IndexView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.grpNotifications);
      this.Controls.Add(this.btnLijstKunstwerken);
      this.Controls.Add(this.txtArtiestNaam);
      this.Controls.Add(this.lblIndexArtiest);
      this.Controls.Add(this.btnExtraInfoKunstwerk);
      this.Controls.Add(this.txtKunstwerkNr);
      this.Controls.Add(this.lblIndexNummerKunstwerk);
      this.Controls.Add(this.lblIndexTitel);
      this.Name = "IndexView";
      this.Size = new System.Drawing.Size(700, 300);
      this.Load += new System.EventHandler(this.IndexView_Load);
      this.grpNotifications.ResumeLayout(false);
      this.grpNotifications.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblIndexTitel;
    private System.Windows.Forms.Label lblIndexNummerKunstwerk;
    private System.Windows.Forms.TextBox txtKunstwerkNr;
    private System.Windows.Forms.Button btnExtraInfoKunstwerk;
    private System.Windows.Forms.Label lblIndexArtiest;
    private System.Windows.Forms.TextBox txtArtiestNaam;
    private System.Windows.Forms.Button btnLijstKunstwerken;
    private System.Windows.Forms.GroupBox grpNotifications;
    private System.Windows.Forms.Label lblNotificatie;
  }
}
