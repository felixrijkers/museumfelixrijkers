﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audiogids
{
  public class Controller
  {
    // Variabelen
    private static Controller instance = new Controller();
    private Archief archief;
    public bool found = false;

    public static Controller Instance
    {
      get { return instance; }
    }

    // Constructor
    private Controller()
    {
      archief = new Archief();
    }

    // Methods
    public IndexView getIndexView()
    {
      IndexView view = new IndexView();
      archief.addObserver(view);
      return view;
    }

    public KunstwerkView getKunstwerkView()
    {
      KunstwerkView view = new KunstwerkView();
      archief.addObserver(view);
      return view;
    }
    
    public LijstKunstwerkenView getLijstView()
    {
      LijstKunstwerkenView view = new LijstKunstwerkenView();
      archief.addObserver(view);
      return view;
    }
    
     

    public void btnBekijkExtraInfoClicked(int nummer)
    {
      // Set passed number to archive number
      archief.nummer = nummer;
      archief.IndexViewIsVisible = false;
      archief.LijstViewIsVisible = false; 
      archief.KunstwerkViewIsVisible = true;
      
      
    }
   
    public void btnBekijkLijstKunstwerkenCLicked(string naam)
    {
      archief.naam = naam;
      archief.LijstViewIsVisible = true;
      archief.IndexViewIsVisible = false;
    } 

    public void btnHomeClicked()
    {
      
      archief.KunstwerkViewIsVisible = false;
      archief.LijstViewIsVisible = false;
      archief.IndexViewIsVisible = true;
      

    }

    public void setNummer(int nummer)
    {
      archief.nummer = nummer;
    }
   

    public bool IndexViewVisibility()
    {
      if (archief.IndexViewIsVisible)
      {
        return true; 
      }
      else
      {
        return false;
      }
    }

    public bool KunstwerkViewVisibility()
    {
      if (archief.KunstwerkViewIsVisible)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

  
    public bool KunstwerkLijstViewVisibility()
    {
      if (archief.LijstViewIsVisible)
      {
        return true;
      }
      else
      {
        return false;
      }
    } 
     
    public string GetKunstwerkNr()
    {
      return archief.GetKunstwerk().GetNummer().ToString();
    }

    public string GetKunstwerkNaam()
    {
      return archief.GetKunstwerk().GetNaam();
    }

    public string GetKunstenaarNaam()
    {
      return archief.GetKunstwerk().GetKunstenaar().GetNaam();
    }

    public string GetKunstenaarGeboortejaar()
    {
      return archief.GetKunstwerk().GetKunstenaar().GetGeboortejaar().ToString();
    }

    public string GetKunstwerkBeschrijving()
    {
      return archief.GetKunstwerk().GetUitleg();
    }
    public string GetZoekterm()
    {
      return archief.naam;
    }  
   public int getNummer()
    {
      return archief.aantalrecords;
    }
  }
}
