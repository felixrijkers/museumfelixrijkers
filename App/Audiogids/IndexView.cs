﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Audiogids
{
  public partial class IndexView : UserControl, Observer
  {


    public IndexView()
    {
      InitializeComponent();
    }

    private void btnBekijkExtraInfo_Click(object sender, EventArgs e)
    {
      if (txtKunstwerkNr.Text == "")
      {
        lblNotificatie.Visible = true;
        lblNotificatie.Text = "Gelieve iets in te vullen aub.";
      }
      else if (Convert.ToInt32(txtKunstwerkNr.Text) > Controller.Instance.getNummer())
      {
        lblNotificatie.Visible = true;
        lblNotificatie.Text =  "Het door u ingevoerde nummer is niet terug gevonden, probeer opniew";
        txtKunstwerkNr.Text = "";
      }
      else
      {
        Controller.Instance.btnBekijkExtraInfoClicked(Convert.ToInt32(txtKunstwerkNr.Text));
      }
    }

    public void ModelChanged()
    {
      txtKunstwerkNr.Clear();
      txtArtiestNaam.Clear();
      lblNotificatie.Visible = false;
      this.Visible = Controller.Instance.IndexViewVisibility();

    }

    private void lblIndexTitel_Click(object sender, EventArgs e)
    {

    }

    private void txtKunstenaar_TextChanged(object sender, EventArgs e)
    {

    }

    private void lblIndexTitle_Click(object sender, EventArgs e)
    {

    }

    private void btnLijstKunstwerken_Click(object sender, EventArgs e)
    {
      if (txtArtiestNaam.Text == "")
      {
        lblNotificatie.Visible = true;
        lblNotificatie.Text = "Gelieve iets in te vullen aub.";
      }
      else
      {
        Controller.Instance.btnBekijkLijstKunstwerkenCLicked(Convert.ToString(txtArtiestNaam.Text));
      }

    }

    private void txtArtiestNaam_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtKunstwerkNr_TextChanged(object sender, EventArgs e)
    {

    }

    private void IndexView_Load(object sender, EventArgs e)
    {

    }
  }
}
