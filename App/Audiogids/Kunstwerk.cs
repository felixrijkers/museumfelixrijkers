﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audiogids
{
  public class Kunstwerk : Observable
  {
    private int nummer;
    private string naam;
    private string uitleg;
    private Kunstenaar kunstenaar;


    public Kunstwerk(int setNummer, string setNaam, string setUitleg, Kunstenaar setKuntenaar)
    {
      this.nummer = setNummer;
      this.naam = setNaam;
      this.uitleg = setUitleg;
      this.kunstenaar = setKuntenaar;
    }

    public int GetNummer()
    {
      return this.nummer;
    }

    public string GetUitleg()
    {
      return this.uitleg;
    }

    public Kunstenaar GetKunstenaar()
    {
      return this.kunstenaar;
    }

    public string GetNaam()
    {
      return this.naam;
    }
  }
}
