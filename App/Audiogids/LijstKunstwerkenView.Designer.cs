﻿namespace Audiogids
{
  partial class LijstKunstwerkenView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnLijstTerugIndex = new System.Windows.Forms.Button();
      this.lstKunstwerken = new System.Windows.Forms.ListBox();
      this.picFotoArtiest = new System.Windows.Forms.PictureBox();
      this.lblArtiestNaam = new System.Windows.Forms.Label();
      this.lblGeboortejaar = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.picFotoArtiest)).BeginInit();
      this.SuspendLayout();
      // 
      // btnLijstTerugIndex
      // 
      this.btnLijstTerugIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnLijstTerugIndex.Location = new System.Drawing.Point(3, 4);
      this.btnLijstTerugIndex.Name = "btnLijstTerugIndex";
      this.btnLijstTerugIndex.Size = new System.Drawing.Size(304, 30);
      this.btnLijstTerugIndex.TabIndex = 21;
      this.btnLijstTerugIndex.Text = "TERUG NAAR INDEX";
      this.btnLijstTerugIndex.UseVisualStyleBackColor = true;
      this.btnLijstTerugIndex.Click += new System.EventHandler(this.btnLijstTerugIndex_Click);
      // 
      // lstKunstwerken
      // 
      this.lstKunstwerken.FormattingEnabled = true;
      this.lstKunstwerken.Location = new System.Drawing.Point(313, 4);
      this.lstKunstwerken.Name = "lstKunstwerken";
      this.lstKunstwerken.Size = new System.Drawing.Size(384, 290);
      this.lstKunstwerken.TabIndex = 23;
      // 
      // picFotoArtiest
      // 
      this.picFotoArtiest.Location = new System.Drawing.Point(3, 44);
      this.picFotoArtiest.Name = "picFotoArtiest";
      this.picFotoArtiest.Size = new System.Drawing.Size(304, 200);
      this.picFotoArtiest.TabIndex = 24;
      this.picFotoArtiest.TabStop = false;
      // 
      // lblArtiestNaam
      // 
      this.lblArtiestNaam.AutoSize = true;
      this.lblArtiestNaam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblArtiestNaam.Location = new System.Drawing.Point(3, 256);
      this.lblArtiestNaam.Name = "lblArtiestNaam";
      this.lblArtiestNaam.Size = new System.Drawing.Size(116, 24);
      this.lblArtiestNaam.TabIndex = 25;
      this.lblArtiestNaam.Text = "Naam Artiest";
      // 
      // lblGeboortejaar
      // 
      this.lblGeboortejaar.AutoSize = true;
      this.lblGeboortejaar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblGeboortejaar.Location = new System.Drawing.Point(219, 262);
      this.lblGeboortejaar.Name = "lblGeboortejaar";
      this.lblGeboortejaar.Size = new System.Drawing.Size(88, 16);
      this.lblGeboortejaar.TabIndex = 26;
      this.lblGeboortejaar.Text = "Geboortejaar";
      // 
      // LijstKunstwerkenView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblGeboortejaar);
      this.Controls.Add(this.lblArtiestNaam);
      this.Controls.Add(this.picFotoArtiest);
      this.Controls.Add(this.lstKunstwerken);
      this.Controls.Add(this.btnLijstTerugIndex);
      this.Name = "LijstKunstwerkenView";
      this.Size = new System.Drawing.Size(700, 300);
      this.Load += new System.EventHandler(this.LijstKunstwerkenView_Load);
      ((System.ComponentModel.ISupportInitialize)(this.picFotoArtiest)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnLijstTerugIndex;
    private System.Windows.Forms.ListBox lstKunstwerken;
    private System.Windows.Forms.PictureBox picFotoArtiest;
    private System.Windows.Forms.Label lblArtiestNaam;
    private System.Windows.Forms.Label lblGeboortejaar;

  }
}
