﻿namespace Audiogids
{
  partial class KunstwerkView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnKunstwerkTerugIndex = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.txtKunstenaar = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.txtJaar = new System.Windows.Forms.TextBox();
      this.lblBeschrijving = new System.Windows.Forms.Label();
      this.rtbBeschrijving = new System.Windows.Forms.RichTextBox();
      this.picFotoKunstwerk = new System.Windows.Forms.PictureBox();
      this.txtNaamKunstwerk = new System.Windows.Forms.TextBox();
      this.lblKunstwerkNr = new System.Windows.Forms.Label();
      this.btnAudio = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.picFotoKunstwerk)).BeginInit();
      this.SuspendLayout();
      // 
      // btnKunstwerkTerugIndex
      // 
      this.btnKunstwerkTerugIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnKunstwerkTerugIndex.Location = new System.Drawing.Point(7, 7);
      this.btnKunstwerkTerugIndex.Name = "btnKunstwerkTerugIndex";
      this.btnKunstwerkTerugIndex.Size = new System.Drawing.Size(330, 30);
      this.btnKunstwerkTerugIndex.TabIndex = 9;
      this.btnKunstwerkTerugIndex.Text = "Home";
      this.btnKunstwerkTerugIndex.UseVisualStyleBackColor = true;
      this.btnKunstwerkTerugIndex.Click += new System.EventHandler(this.btnKunstwerkTerugIndex_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(4, 234);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(52, 18);
      this.label2.TabIndex = 10;
      this.label2.Text = "Naam:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(354, 11);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(87, 18);
      this.label3.TabIndex = 12;
      this.label3.Text = "Kunstenaar:";
      // 
      // txtKunstenaar
      // 
      this.txtKunstenaar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtKunstenaar.Location = new System.Drawing.Point(355, 34);
      this.txtKunstenaar.Name = "txtKunstenaar";
      this.txtKunstenaar.Size = new System.Drawing.Size(326, 24);
      this.txtKunstenaar.TabIndex = 13;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(354, 71);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(41, 18);
      this.label4.TabIndex = 14;
      this.label4.Text = "Jaar:";
      // 
      // txtJaar
      // 
      this.txtJaar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtJaar.Location = new System.Drawing.Point(355, 94);
      this.txtJaar.Name = "txtJaar";
      this.txtJaar.Size = new System.Drawing.Size(326, 24);
      this.txtJaar.TabIndex = 15;
      // 
      // lblBeschrijving
      // 
      this.lblBeschrijving.AutoSize = true;
      this.lblBeschrijving.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblBeschrijving.Location = new System.Drawing.Point(352, 130);
      this.lblBeschrijving.Name = "lblBeschrijving";
      this.lblBeschrijving.Size = new System.Drawing.Size(49, 18);
      this.lblBeschrijving.TabIndex = 16;
      this.lblBeschrijving.Text = "Uitleg:";
      // 
      // rtbBeschrijving
      // 
      this.rtbBeschrijving.Location = new System.Drawing.Point(355, 152);
      this.rtbBeschrijving.Name = "rtbBeschrijving";
      this.rtbBeschrijving.Size = new System.Drawing.Size(326, 133);
      this.rtbBeschrijving.TabIndex = 17;
      this.rtbBeschrijving.Text = "";
      // 
      // picFotoKunstwerk
      // 
      this.picFotoKunstwerk.Location = new System.Drawing.Point(7, 43);
      this.picFotoKunstwerk.Name = "picFotoKunstwerk";
      this.picFotoKunstwerk.Size = new System.Drawing.Size(330, 155);
      this.picFotoKunstwerk.TabIndex = 19;
      this.picFotoKunstwerk.TabStop = false;
      // 
      // txtNaamKunstwerk
      // 
      this.txtNaamKunstwerk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.txtNaamKunstwerk.Location = new System.Drawing.Point(7, 261);
      this.txtNaamKunstwerk.Name = "txtNaamKunstwerk";
      this.txtNaamKunstwerk.Size = new System.Drawing.Size(330, 24);
      this.txtNaamKunstwerk.TabIndex = 11;
      // 
      // lblKunstwerkNr
      // 
      this.lblKunstwerkNr.AutoSize = true;
      this.lblKunstwerkNr.Location = new System.Drawing.Point(4, 205);
      this.lblKunstwerkNr.Name = "lblKunstwerkNr";
      this.lblKunstwerkNr.Size = new System.Drawing.Size(37, 13);
      this.lblKunstwerkNr.TabIndex = 20;
      this.lblKunstwerkNr.Text = "00000";
      // 
      // btnAudio
      // 
      this.btnAudio.Location = new System.Drawing.Point(261, 205);
      this.btnAudio.Name = "btnAudio";
      this.btnAudio.Size = new System.Drawing.Size(75, 23);
      this.btnAudio.TabIndex = 21;
      this.btnAudio.Text = "Speel Audio";
      this.btnAudio.UseVisualStyleBackColor = true;
      // 
      // KunstwerkView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnAudio);
      this.Controls.Add(this.lblKunstwerkNr);
      this.Controls.Add(this.picFotoKunstwerk);
      this.Controls.Add(this.rtbBeschrijving);
      this.Controls.Add(this.lblBeschrijving);
      this.Controls.Add(this.txtJaar);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.txtKunstenaar);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtNaamKunstwerk);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.btnKunstwerkTerugIndex);
      this.Name = "KunstwerkView";
      this.Size = new System.Drawing.Size(700, 300);
      this.Load += new System.EventHandler(this.KunstwerkView_Load_1);
      ((System.ComponentModel.ISupportInitialize)(this.picFotoKunstwerk)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnKunstwerkTerugIndex;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txtKunstenaar;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox txtJaar;
    private System.Windows.Forms.Label lblBeschrijving;
    private System.Windows.Forms.RichTextBox rtbBeschrijving;
    private System.Windows.Forms.PictureBox picFotoKunstwerk;
    private System.Windows.Forms.TextBox txtNaamKunstwerk;
    private System.Windows.Forms.Label lblKunstwerkNr;
    private System.Windows.Forms.Button btnAudio;

  }
}
